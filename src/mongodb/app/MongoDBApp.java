package mongodb.app;

import com.formdev.flatlaf.FlatLightLaf;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import javax.swing.UIManager;
import mongodb.app.ui.LoginForm;
import mongodb.app.utils.CustomFont;

public class MongoDBApp {

    public static void main(String[] args) {
        try {
//            MongoClientOptions.Builder builder = MongoClientOptions.builder();
            ServerAddress serverAddress = new ServerAddress("localhost", 27017);
            MongoClient mongo = new MongoClient(serverAddress);

            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    UIManager.put("defaultFont", CustomFont.getFont());
                    FlatLightLaf.setup();
                    new LoginForm(mongo).setVisible(true);
                }
            });

        } catch (MongoException e) {
            System.out.println("Error: MongoDB connection failed. Please make sure MongoDB is running.");
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
