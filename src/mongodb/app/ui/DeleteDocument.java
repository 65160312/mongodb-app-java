package mongodb.app.ui;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.util.JSON;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import org.bson.Document;

public class DeleteDocument extends javax.swing.JPanel {

    private MongoCollection collection;

    public DeleteDocument(MongoClient mongo) {
        initComponents();
        this.collection = mongo.getDatabase("BigC").getCollection("Employee");

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabelResult = new javax.swing.JLabel();
        jLabelShowDocs = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListShowDocs = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListSelectDocs = new javax.swing.JList<>();
        jBtnShowAll = new javax.swing.JButton();
        jBtnSearch = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListResults = new javax.swing.JList<>();
        jBtnCancel = new javax.swing.JButton();
        jTFsearchField = new javax.swing.JTextField();
        jLabelEmpID = new javax.swing.JLabel();
        jLabelEmpID1 = new javax.swing.JLabel();
        jTFdeleteField = new javax.swing.JTextField();
        jBtnDelete = new javax.swing.JButton();
        jBtnExit = new javax.swing.JButton();

        jLabel1.setText("เลือกเอกสารที่ต้องการลบ");

        jLabelResult.setText("เอกสารทั้งหมดหลังลบ");

        jLabelShowDocs.setText("เอกสารทั้งหมด");

        jListShowDocs.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListShowDocsValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jListShowDocs);

        jListSelectDocs.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListSelectDocsValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jListSelectDocs);

        jBtnShowAll.setText("แสดง");
        jBtnShowAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnShowAllActionPerformed(evt);
            }
        });

        jBtnSearch.setText("ค้นหา");
        jBtnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnSearchActionPerformed(evt);
            }
        });

        jScrollPane3.setViewportView(jListResults);

        jBtnCancel.setText("ยกเลิก");
        jBtnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCancelActionPerformed(evt);
            }
        });

        jLabelEmpID.setText("รหัสพนักงาน (emp_id)  :");

        jLabelEmpID1.setText("รหัสพนักงาน (emp_id) :");

        jBtnDelete.setText("ลบ");
        jBtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnDeleteActionPerformed(evt);
            }
        });

        jBtnExit.setText("Exit");
        jBtnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnExitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addComponent(jScrollPane2)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabelShowDocs)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jBtnShowAll))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabelResult))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 174, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabelEmpID)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTFsearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBtnSearch))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabelEmpID1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTFdeleteField, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBtnCancel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jBtnDelete))
                            .addComponent(jBtnExit, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnShowAll)
                    .addComponent(jLabelShowDocs))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnSearch)
                    .addComponent(jTFsearchField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEmpID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtnDelete)
                    .addComponent(jTFdeleteField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelEmpID1)
                    .addComponent(jBtnCancel))
                .addGap(12, 12, 12)
                .addComponent(jLabelResult)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jBtnExit)
                .addContainerGap(14, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jListShowDocsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListShowDocsValueChanged
        //รับค่าจาก doc ที่เลือก ไปแสดงที่ search field
        try {
            Document selectDoc = Document.parse(jListShowDocs.getSelectedValue());
            jTFsearchField.setText(selectDoc.getString("emp_id"));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Please select document fist",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jListShowDocsValueChanged

    private void jBtnShowAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnShowAllActionPerformed
        //สร้าง model และ เรียก collection ทั้งหมด แล้วใช้ cursor ชี้เพื่อเก็บ element
        //เข้า model ทีละตัว แล้ว set model ให้กับ JList
        DefaultListModel model = new DefaultListModel();
        MongoCursor cursor = this.collection.find().iterator();
        while (cursor.hasNext()) {
            model.addElement(JSON.serialize(cursor.next()));
        }
        jListShowDocs.setModel(model);
    }//GEN-LAST:event_jBtnShowAllActionPerformed

    private void jBtnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnSearchActionPerformed
        //สร้าง model และ เรียก collection ทั้งหมด โดยมี filter เป็น emp_id 
        //แล้วใช้ cursor ชี้เพื่อเก็บ element เข้า model ทีละตัว แล้ว set model ให้กับ JList
        DefaultListModel model = new DefaultListModel();
        Document filter = new Document();
        filter.append("emp_id", jTFsearchField.getText());
        MongoCursor cursor = this.collection.find().filter(filter).iterator();
        while (cursor.hasNext()) {
            model.addElement(JSON.serialize(cursor.next()));
        }
        jListSelectDocs.setModel(model);
    }//GEN-LAST:event_jBtnSearchActionPerformed

    private void jBtnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnDeleteActionPerformed
        //รับค่าจาก text field ไป query ด้วย emp_id เพื่อลบ
        String field = jTFdeleteField.getText();
        if (!field.isEmpty()) {
            DeleteResult result = collection.deleteOne(new Document().append("emp_id",
                    field));
            if (result.getDeletedCount() > 0) {
                DefaultListModel model = new DefaultListModel();
                MongoCursor cursor = this.collection.find().iterator();
                while (cursor.hasNext()) {
                    model.addElement(JSON.serialize(cursor.next()));
                }
                jListResults.setModel(model);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Please input employee ID !",
                    "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jBtnDeleteActionPerformed

    private void jBtnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnExitActionPerformed
        //ปิดหน้านี้
        setVisible(false);
    }//GEN-LAST:event_jBtnExitActionPerformed

    private void jListSelectDocsValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListSelectDocsValueChanged
        //รับค่าจาก doc ที่เลือก ไปแสดงที่ text field 
        try {
            Document selectDoc = Document.parse(jListSelectDocs.getSelectedValue());
            jTFdeleteField.setText(selectDoc.getString("emp_id"));
        } catch (Exception e) {
            System.out.println(e);
            JOptionPane.showMessageDialog(this, "Please select doc first !",
                    "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jListSelectDocsValueChanged

    private void jBtnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCancelActionPerformed
        //ล้างค่าใน text field และ selection list
        jListSelectDocs.clearSelection();
        jTFdeleteField.setText("");
    }//GEN-LAST:event_jBtnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnCancel;
    private javax.swing.JButton jBtnDelete;
    private javax.swing.JButton jBtnExit;
    private javax.swing.JButton jBtnSearch;
    private javax.swing.JButton jBtnShowAll;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelEmpID;
    private javax.swing.JLabel jLabelEmpID1;
    private javax.swing.JLabel jLabelResult;
    private javax.swing.JLabel jLabelShowDocs;
    private javax.swing.JList<String> jListResults;
    private javax.swing.JList<String> jListSelectDocs;
    private javax.swing.JList<String> jListShowDocs;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField jTFdeleteField;
    private javax.swing.JTextField jTFsearchField;
    // End of variables declaration//GEN-END:variables
}
