package mongodb.app.ui;

import java.awt.Container;
import javax.swing.JOptionPane;

public class DialogsUI {

    public static void openWarning(Container container,String title, String msg) {
        JOptionPane.showMessageDialog(container, msg, title, JOptionPane.WARNING_MESSAGE);
    }
}
