package mongodb.app.utils;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.io.File;

public class CustomFont {

    private static Font font = getFont();

    public static void setGlobalFont(Container container) {
        for (Component component : container.getComponents()) {
            component.setFont(font);
            if (component instanceof Container) {
                setGlobalFont((Container) component);
            }
        }
    }

    public static Font getFont() {
        try {
            return Font.createFont(Font.TRUETYPE_FONT, new File("src/resource/fonts/Kanit-Regular.ttf")).deriveFont(Font.PLAIN, 14);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    public static void setFontComponents(Component[] components) {
        for (Component comp : components) {
            comp.setFont(font);
        }
    }

}
